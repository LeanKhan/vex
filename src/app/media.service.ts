import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "../environments/environment";
import { api } from "../config/api-config";

@Injectable({
  providedIn: "root"
})
export class MediaService {
  constructor(private _http: HttpClient) {}

  // Variables

  public showLoader: boolean = false;

  private api_uri = `${api.address}:${api.port}`;

  private _tmdb_api_key = environment.tmdb_api_key;

  search_results$: any;

  /*
  ======== Methods ========
  */

  /**
   * Search TMDB Database
   *
   * Uses the /search endpoint of TMDB to return a list
   * of movies, people, series that match a given query.
   *
   * but we only want movies and series :)
   */

  searchTMDB(query) {
    this.showLoader = !this.showLoader;
    const params = new HttpParams()
      .set("api_key", this._tmdb_api_key)
      .set("query", query);
    this._http
      .get(`https://api.themoviedb.org/3/search/multi`, { params })
      .subscribe(
        result => {
          // Remove results that have media_type = 'person';
          let results: any[] = result["results"];

          this.search_results$ = results.filter(this.check_media_type);

          // console.log(this.search_results$);
        },
        err => {
          throw err;
        },
        () => {
          setTimeout(() => {
            this.showLoader = !this.showLoader;
          }, 1500);
        }
      ),
      err => {
        console.log(err);
      };
  }

  /**
   * Upload media
   */

  upload(media_type, id) {
    let payload;
    const params = new HttpParams().set("api_key", this._tmdb_api_key);

    this._http
      .get(`https://api.themoviedb.org/3/${media_type}/${id}`, { params })
      .subscribe(
        movie => {
          payload = movie;
          console.log(payload);
        },
        err => {
          console.log(err);
        },
        () => {
          this._http
            .post("http://localhost:3000/api/v1/media/movies/add", payload)
            .subscribe(movie => console.log(movie));
        }
      );
  }

  /*
  ========== Callback Functions ==========
  */

  /**
   * Check Media Type
   *
   * This function checks if the media type of search result is 'movie' or 'tv'
   *
   * If not return false
   *
   * Used in searchTMDB() function
   */

  check_media_type(result): boolean {
    if (result.media_type == "movie" || result.media_type == "tv") {
      return true;
    } else {
      return false;
    }
  }

  // get data
  getSeries() {
    return this._http.get(`http://${this.api_uri}/api/shows`);
  }
}
