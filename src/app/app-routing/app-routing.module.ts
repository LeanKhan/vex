import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

// Import Components here:

import { UserComponent } from "../user/user.component";
import { DiscoverComponent } from "../user/discover/discover.component";
import { MediaInfoComponent } from "../user/media-info/media-info.component";
import { MediaListComponent } from "../media-list/media-list.component";
import { ValidationComponent } from "../validation/validation.component";
import { VideoplayerMoviesComponent } from "../videoplayer/videoplayer-movies.component";
import { VideoplayerSeriesComponent } from "../videoplayer-series/videoplayer-series.component";
import { ChannelViewComponent } from "../channel-view/channel-view.component";
import { LoginComponent } from "../validation/login/login.component";
import { RegisterComponent } from "../validation/register/register.component";
import { AdminComponent } from "../admin/admin.component";
import { DashboardComponent } from "../admin/dashboard/dashboard.component";
import {ListComponent} from "../navbar/list/list.component";
import {ErrorComponent} from "../error/error.component";

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "u" },
  {
    path: "u",
    component: UserComponent,
    children: [
      { path: "discover", component: DiscoverComponent },
      { path: "view/:id", component: MediaInfoComponent },
      { path: "media/:mediatype", component: MediaListComponent },
      { path: "media/:mediatype", component: MediaListComponent },
      {path: "search/:query", component: ListComponent},
      { path: "movie/:id", component: VideoplayerMoviesComponent },
      { path: "series/:id", component: VideoplayerSeriesComponent },
      { path: "channel/:id", component: ChannelViewComponent },
      { path: "", pathMatch: "full", redirectTo: "discover" }
    ]
  },

  { 
    path: "validate", 
    component: ValidationComponent,
    children: [
      {path: "login", component: LoginComponent},
      {path: "register", component: RegisterComponent},
      { path: "", pathMatch: "full", redirectTo: "login" }
    ]
  },
  {path: "admin", component: AdminComponent, children: [
    {path: 'dashboard', component: DashboardComponent},
    {path: "", pathMatch: "full", redirectTo: "dashboard"}
  ]},
  {path: "**", pathMatch: "full", component: ErrorComponent}
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {}
