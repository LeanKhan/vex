import { Component, OnInit } from "@angular/core";
import { UiService as ui } from "../ui.service";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css"]
})
export class AdminComponent implements OnInit {
  constructor(public _ui: ui) {}

  ngOnInit() {}
}
