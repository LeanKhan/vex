import { Component, OnInit } from '@angular/core';
import {UiService as UI} from "../ui.service";
import {ActivatedRoute} from "@angular/router";
import {MaterializeDirective} from 'angular2-materialize';


@Component({
  selector: 'app-channel-view',
  templateUrl: './channel-view.component.html',
  styleUrls: ['./channel-view.component.css']
})
export class ChannelViewComponent implements OnInit {
  channel={
    title: "The Flash",
    members: 256,
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam maximus lectus viverra augue malesuada mattis. In pretium fermentum blandit. Etiam cursus nisl sit amet posuere rutrum. Sed sed fermentum erat. Praesent mattis ex posuere turpis bibendum sodales. Quisque dapibus vitae nibh vitae scelerisque. Vivamus et erat tellus. Suspendisse vehicula erat nec faucibus fermentum. Donec semper nibh in dui pellentesque tincidunt. Nulla iaculis ac nibh id vulputate. In mauris justo, bibendum et vestibulum a, euismod quis quam. Donec lacus urna, varius et congue vitae, lobortis ac ipsum.",
    date: "January 2, 2018",
    creator: "Jake Austin",
    likes: 4067
  };
  episodes = 67;
  members = 249;
  constructor(private _ui:UI, private _thisRoute:ActivatedRoute) {}

  ngOnInit() {
    this._thisRoute.params.subscribe((params)=>{
      console.log(params.id);
    })
  }

}
