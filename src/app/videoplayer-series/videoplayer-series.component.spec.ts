import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoplayerSeriesComponent } from './videoplayer-series.component';

describe('VideoplayerSeriesComponent', () => {
  let component: VideoplayerSeriesComponent;
  let fixture: ComponentFixture<VideoplayerSeriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoplayerSeriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoplayerSeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
