import { Component, OnInit, AfterViewInit } from "@angular/core";
import { MediaService as Media } from "../../media.service";

@Component({
  selector: "app-discover",
  templateUrl: "./discover.component.html",
  styleUrls: ["./discover.component.css"]
})
export class DiscoverComponent implements OnInit, AfterViewInit {
  movieTitle = "Vikings";
  shows;
  constructor(private _media: Media) {}

  ngOnInit() {
    // this.shows = this._media.getSeries();
    // console.log(this._media.getSeries());
    this._media.getSeries().subscribe(shows => {
      this.shows = shows;
      console.log(shows);
    });
  }

  ngAfterViewInit() {
    // console.log(this._media.getSeries());
  }
}
