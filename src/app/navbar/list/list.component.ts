import { Component, OnInit } from "@angular/core";
import { MediaService as Media } from "../../media.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.css"]
})
export class ListComponent implements OnInit {
  query: string;

  constructor(public _media: Media, public _thisRoute: ActivatedRoute) {}

  ngOnInit() {
    /*
      This is kind of a hack.
      So when a user types in the url, everything is fine. But when they
      initiate a new search the url changes quite alright, but the component is * re-used *
      So some of ngOnInit() won't run. So I duplicated the second part of the code in the 
      navbar component (where the search button is). So when a user initiates a new search, _that_ 
      function is run but when they type a new url _this_ function is run. review

      - @leankhan
    */
    this._thisRoute.params.subscribe(
      param => (this.query = param.query),
      err => {
        console.error(err);
      }
    );
    this._media.searchTMDB(this.query);
  }
}
