import { Component, OnInit } from '@angular/core';
import {Chart} from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  
  LineChart;
  PieChart;
  media;
  constructor() { }

  ngOnInit() {
    this.LineChart = new Chart('LineChart',{
      type: 'line',
      data:{
        labels: ["Jan","Feb","March","April","May","June","July","Aug","Sept","Oct","Nov","Dec"],
        datasets: [{
          label: 'Vex Usage',
          data: [200,100,350,400,150,20,50,100,300,400,500,120],
          fill: true,
          pointRadius: 2.5,
          pointBackgroundColor: 'black',
          lineTension: 0.5,
          borderColor: "#17171C",
          borderWidth: 1
        }]
      },
      options: {
        title:{
          text:"Line Chart",
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {beginAtZero: true}
          }]
        }
      }
    })

    this.PieChart = new Chart('PieChart',{
      type: 'pie',
      data:{
        labels: ["Mp4","Mkv","Mp3","Rar"],
        datasets: [{
          label: 'Vex Usage',
          data: [100,153,356,57],
          backgroundColor: ['#17171c','red','orange','violet'],
          borderColor: "#F3F3F3",
          hoverBorderColor: "white",
          borderWidth: 3
        }]
      },
      options: {
        title:{
          text:"Pie Chart",
          display: false
        },
        
      }
    })

    this.media = [{category: 'Movies',amount: '76'}, {category: 'Series',amount: '206'}, {category: 'Music',amount: '166'}];
  }

}
