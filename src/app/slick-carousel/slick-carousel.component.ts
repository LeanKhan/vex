import { Component,Input, ElementRef, AfterViewInit } from '@angular/core';
declare var jQuery: any;
@Component({
  selector: 'slick-carousel',
  template: `<ng-content></ng-content>`
})
export class SlickCarouselComponent implements AfterViewInit{


  $element: any;

  constructor(private el: ElementRef) {}
  ngAfterViewInit() {
    this.$element = jQuery(this.el.nativeElement).slick({dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      prevArrow: "<button type='button' class='slide-prev'><img class='chevron-left' src='assets/img/chevron-left-white.png'></button>",
      nextArrow: "<button type='button' class='slide-next'><img class='chevron-right' src='assets/img/chevron-right-white.png'></button>",
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: false,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]});  
  }
}
