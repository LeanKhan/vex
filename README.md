# Vex Frontend

# 2021 UPDATE:

### This is not 100% my work. I and Edward Rajah worked on this in 2018 to replace a school system

### I'm uploading this here so it won't get lost. Also I had to delete the old `.git` folde because it was too large

Backend hosted on [Gitlab](https://gitlab.com/aun-hub/vex-backend)

## From the beautiful people of AUN-HUB

# Contributing

- NodeJS version : v8.11.3 [website](https://nodejs.org/en/)
- Angular: 6.1.9 [website](https://angular.io)
- Angular/cli: 6.1.5 [website](https://cli.angular.io)
- npm: 6.1.0 [website](https://npmjs.com)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
