import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class UserService {
  apiUri = "http://localhost:3000/api/v1";

  constructor(private _http: HttpClient) {}
  // Endpoints
  addUser(payload) {
    return this._http.post(`${this.apiUri}/users/add`, payload);
  }
  // Login User
  loginUser(payload){
    return this._http.post(`${this.apiUri}/users/login`, payload);
  }
}
