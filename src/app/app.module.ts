import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing/app-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from "./app.component";
import { DiscoverComponent } from "./user/discover/discover.component";
import { UserComponent } from "./user/user.component";
import { MediaInfoComponent } from "./user/media-info/media-info.component";
import { MediaGridComponent } from "./user/media-grid/media-grid.component";
import { MediaListComponent } from "./media-list/media-list.component";
import { LoginComponent } from "./validation/login/login.component";
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { DataComponent } from './admin/data/data.component';
import {SlideshowModule} from 'ng-simple-slideshow';
import { NavbarComponent } from './navbar/navbar.component';
import { ValidationComponent } from './validation/validation.component';
import { RegisterComponent } from './validation/register/register.component';
import { ErrorComponent } from './error/error.component';
import { ListComponent } from './navbar/list/list.component';
import { VideoplayerMoviesComponent } from './videoplayer/videoplayer-movies.component';
import { VideoplayerSeriesComponent } from './videoplayer-series/videoplayer-series.component';
import { MaterializeModule } from 'angular2-materialize';
import { ChannelViewComponent } from './channel-view/channel-view.component';
import { SlickCarouselComponent } from './slick-carousel/slick-carousel.component';


@NgModule({
  declarations: [
    AppComponent,
    DiscoverComponent,
    UserComponent,
    MediaInfoComponent,
    MediaGridComponent,
    MediaListComponent,
    LoginComponent,
    AdminComponent,
    DashboardComponent,
    DataComponent,
    NavbarComponent,
    ValidationComponent,
    RegisterComponent,
    ErrorComponent,
    ListComponent,
    VideoplayerMoviesComponent,
    VideoplayerSeriesComponent,
    ChannelViewComponent,
    SlickCarouselComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterializeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
