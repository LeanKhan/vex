import { Component, OnInit } from "@angular/core";
import {UiService as UI} from "../ui.service";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"]
})
export class UserComponent implements OnInit {
  constructor(public _ui:UI) {}

  admin: boolean = false;

  ngOnInit() {}

}
