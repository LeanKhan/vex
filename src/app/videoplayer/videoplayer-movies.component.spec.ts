import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoplayerMoviesComponent } from './videoplayer-movies.component';

describe('VideoplayerComponent', () => {
  let component: VideoplayerMoviesComponent;
  let fixture: ComponentFixture<VideoplayerMoviesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoplayerMoviesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoplayerMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
