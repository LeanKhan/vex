import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "../../user.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  signInProcess;
  signupForm: FormGroup;
  signinForm: FormGroup;

  constructor(
    private _thisRoute: ActivatedRoute,
    private _userService: UserService,
    private _router: Router
  ) {
    (this.signupForm = new FormGroup({
      username: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      domain_name: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(5)
      ])
    })),
      (this.signinForm = new FormGroup({
        username: new FormControl(null, [
          Validators.required,
          Validators.minLength(3)
        ]),
        password: new FormControl(null, [
          Validators.required,
          Validators.minLength(5)
        ])
      }));
  }

  ngOnInit() {
    // this._thisRoute.data.subscribe(data => {
    //   this.signInProcess = data["user"];
    // });
  }

  // Get FormGroups

  /* ===================== */

  get getSignInForm() {
    return this.signinForm;
  }
  // Get signup form values
  get getSignUpForm() {
    return this.signupForm;
  }

  /* ====================== */

  // Get SignIn FormControls

  /* ==================== */

  // Get signin Username

  get getSignInUsername() {
    return this.signinForm.get("username");
  }
  // Get signin password
  get getSignInPassword() {
    return this.signinForm.get("password");
  }

  /* =================== */

  //  Get SignUp FormControls

  /* ======================== */

  // Get signup username
  get getSignUpUsername() {
    return this.signupForm.get("username");
  }
  // Get signup email
  get getSignUpDomain() {
    return this.signupForm.get("domain_name");
  }
  // Get signup password
  get getSignUpPassword() {
    return this.signupForm.get("password");
  }

  /* ===-=-=-=-=-=-=-===============*/

  // Clear form
  clearForm() {
    this.signinForm.setValue({
      username: null,
      password: null
    });
    this.signupForm.setValue({
      username: null,
      password: null,
      domain_name: null
    });
  }

  //  Go to login page after signup

  goToLogin() {
    this._router.navigateByUrl("/signin");
  }

  // Add user
  addUser(payload) {
    let now = new Date();
    this._userService.addUser(payload).subscribe(
      res => {
        console.log(res);
      },
      err => {
        if (err) console.error(err);
      },
      () => {
        this.goToLogin();
      }
    );
    this.clearForm();
  }

  // Login User
  loginUser(payload) {
    this._userService.loginUser(payload).subscribe(res => {
      localStorage.setItem("userTokenDetails", JSON.stringify(res));
      console.log(res);
    });
    this.clearForm();
  }
}
