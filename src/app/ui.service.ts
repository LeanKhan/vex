import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UiService {

  constructor() { }

// Sidebar is-closed variable closed

menuClosed:boolean =true;
// Function to toggle sidebar

toggleMenu(): void {
  this.menuClosed = !this.menuClosed;
}
}
