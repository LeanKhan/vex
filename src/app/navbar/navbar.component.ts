import { Component, OnInit } from "@angular/core";
import { UiService as UI } from "../ui.service";
import { MediaService as Media } from "../media.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  // Show progress loader or not

  constructor(
    public _ui: UI,
    public _media: Media,
    public _router: Router,
    public _thisRoute: ActivatedRoute
  ) {}

  ngOnInit() {}

  // Functions
  searchTMDB(query) {
    this._router.navigate(["./search", `${query}`], {
      relativeTo: this._thisRoute
    });
    this._media.searchTMDB(query);
  }
}
