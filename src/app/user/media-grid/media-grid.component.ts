import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-media-grid",
  templateUrl: "./media-grid.component.html",
  styleUrls: ["./media-grid.component.css"]
})
export class MediaGridComponent implements OnInit {
  // Variables

  constructor() {}

  ngOnInit() {}

  // Functions

  /** Open modal */

  openModal(e: Event, modal: HTMLElement) {
    modal.style.display = "block";
  }

  closeModal(e: Event, modal: HTMLElement) {
    if (e.target == modal || e.target == modal.firstChild) {
      modal.style.display = "none";
    }
  }
}
