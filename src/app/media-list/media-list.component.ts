import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-media-list",
  templateUrl: "./media-list.component.html",
  styleUrls: ["./media-list.component.css"]
})
export class MediaListComponent implements OnInit {
  mediaType;

  constructor(private _thisRoute: ActivatedRoute) {}

  ngOnInit() {
    this._thisRoute.paramMap.subscribe(p => {
      this.mediaType = p["params"].mediatype;
    });
  }
}
